bub
===

bubble-slurping platform puzzle game

play on the web:  
http://bub.penduin.net  

play via Smile BASIC on Nintendo 3DS:  
public key = 83DXN3L4  
https://web.archive.org/web/20171025045657/https://miiverse.nintendo.net/posts/AYMHAAACAAADVHksVo-stg  
http://smilebasicsource.com/page?pid=155  
