LOAD({
	"name": "door",
	"scale": 0.4,
	"above": [
		{
			"name": "door1",
			"image": "image/scribble/door.png",
			"pivot": {
				"x": 156,
				"y": 341
			},
			"rotate": 2,
			"scale": 1,
			"alpha": 1,
			"offset": {
				"x": 0,
				"y": 0
			},
			"above": [],
			"below": []
		}
	],
	"below": [],
	"pose": {}
});
