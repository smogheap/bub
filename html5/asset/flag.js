LOAD({
	"name": "flag",
	"scale": 0.3,
	"above": [
		{
			"name": "pole",
			"image": "image/scribble/flag-pole.png",
			"pivot": {
				"x": 121,
				"y": 397
			},
			"rotate": -5,
			"scale": 1,
			"alpha": 1,
			"offset": {
				"x": 0,
				"y": 0
			},
			"above": [],
			"below": [
				{
					"name": "1",
					"image": "image/scribble/flag-1.png",
					"pivot": {
						"x": 15,
						"y": 77
					},
					"rotate": 6,
					"scale": 1,
					"alpha": 1,
					"offset": {
						"x": 158,
						"y": 100
					},
					"above": [
						{
							"name": "2",
							"image": "image/scribble/flag-2.png",
							"pivot": {
								"x": 8,
								"y": 44
							},
							"rotate": 1,
							"scale": 1.3,
							"alpha": 1,
							"offset": {
								"x": 106,
								"y": 68
							},
							"above": [
								{
									"name": "3",
									"image": "image/scribble/flag-3.png",
									"pivot": {
										"x": 8,
										"y": 30
									},
									"rotate": 6,
									"scale": 1.3,
									"alpha": 1,
									"offset": {
										"x": 64,
										"y": 38
									},
									"above": [],
									"below": []
								},
								{
									"name": "shade",
									"image": "image/scribble/flag-shade.png",
									"pivot": {
										"x": 78,
										"y": 84
									},
									"rotate": -7,
									"scale": 0.8,
									"alpha": 1,
									"offset": {
										"x": -16,
										"y": 47
									},
									"above": [],
									"below": []
								}
							],
							"below": []
						}
					],
					"below": []
				}
			]
		}
	],
	"below": [],
	"pose": {}
});