LOAD({
	"name": "left",
	"scale": 0.25,
	"above": [
		{
			"name": "sign",
			"image": "image/scribble/sign.png",
			"pivot": {
				"x": 232,
				"y": 512
			},
			"rotate": 0,
			"scale": 1,
			"alpha": 1,
			"offset": {
				"x": 24,
				"y": 0
			},
			"above": [],
			"below": []
		}
	],
	"flipx": true,
	"below": [],
	"pose": {}
});
