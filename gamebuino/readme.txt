create a folder "bub" in your arduino sketchbook and copy these .ino files
(.c really, but arduino dev likes to be cute)

should be able to build and send to your device and/or dig bub.cpp.hex out of
the build directory (file->preferences, show verbose output during compilation
to find it), rename it BUB.HEX, and copy to microSD card
