;
; iNES header
;

.segment "HEADER"

INES_MAPPER = 0 ; 0 = NROM
INES_MIRROR = 1 ; 0 = horizontal mirroring, 1 = vertical mirroring
INES_SRAM   = 0 ; 1 = battery backed SRAM at $6000-7FFF

.byte 'N', 'E', 'S', $1A ; ID
.byte $02 ; 16k PRG chunk count
.byte $01 ; 8k CHR chunk count
.byte INES_MIRROR | (INES_SRAM << 1) | ((INES_MAPPER & $f) << 4)
.byte (INES_MAPPER & %11110000)
.byte $0, $0, $0, $0, $0, $0, $0, $0 ; padding

;
; CHR ROM
;

.segment "TILES"
.incbin "background.chr"
.incbin "sprite.chr"

;
; vectors placed at top 6 bytes of memory area
;

.segment "VECTORS"
.word nmi
.word reset
.word irq

;
; reset routine
;

.segment "CODE"
reset:
	sei       ; mask interrupts
	lda #0
	sta $2000 ; disable NMI
	sta $2001 ; disable rendering
	sta $4015 ; disable APU sound
	sta $4010 ; disable DMC IRQ
	lda #$40
	sta $4017 ; disable APU IRQ
	cld       ; disable decimal mode
	ldx #$FF
	txs       ; initialize stack
	; wait for first vblank
	bit $2002
	:
		bit $2002
		bpl :-
	; clear all RAM to 0
	lda #0
	ldx #0
	:
		sta $0000, X
		sta $0100, X
		sta $0200, X
		sta $0300, X
		sta $0400, X
		sta $0500, X
		sta $0600, X
		sta $0700, X
		inx
		bne :-
	; place all sprites offscreen at Y=255
	lda #255
	ldx #0
	:
		sta oam, X
		inx
		inx
		inx
		inx
		bne :-
	; wait for second vblank
	:
		bit $2002
		bpl :-
	; NES is initialized, ready to begin!
	; enable the NMI for graphical updates, and jump to our main program
	lda #%10001000
	sta $2000
	jmp main

	;; stolen from nesdev.com
init_apu:
        ; Init $4000-4013
        ldy #$13
@loop:  lda @regs,y
        sta $4000,y
        dey
        bpl @loop
        ; We have to skip over $4014 (OAMDMA)
        lda #$0f
        sta $4015
        lda #$40
        sta $4017
        rts
@regs:
        .byte $30,$08,$00,$00
        .byte $30,$08,$00,$00
        .byte $80,$00,$00,$00
        .byte $30,$00,$00,$00
        .byte $00,$00,$00,$00

;
; nmi routine
;

.segment "ZEROPAGE"
nmi_lock:       .res 1 ; prevents NMI re-entry
nmi_count:      .res 1 ; is incremented every NMI
nmi_ready:      .res 1 ; set to 1 to push a PPU frame update, 2 to turn rendering off next NMI
nmt_update_len: .res 1 ; number of bytes in nmt_update buffer
scroll_x:       .res 1 ; x scroll position
scroll_y:       .res 1 ; y scroll position
scroll_nmt:     .res 1 ; nametable select (0-3 = $2000,$2400,$2800,$2C00)
temp:           .res 1 ; temporary variable

.segment "BSS"
nmt_update: .res 256 ; nametable update entry buffer for PPU update
palette:    .res 32  ; palette buffer for PPU update

.segment "OAM"
oam: .res 256        ; sprite OAM data to be uploaded by DMA

.segment "CODE"
nmi:
	; save registers
	pha
	txa
	pha
	tya
	pha
	; prevent NMI re-entry
	lda nmi_lock
	beq :+
		jmp @nmi_end
	:
	lda #1
	sta nmi_lock
	; increment frame counter
	inc nmi_count
	;
	lda nmi_ready
	bne :+ ; nmi_ready == 0 not ready to update PPU
		jmp @ppu_update_end
	:
	cmp #2 ; nmi_ready == 2 turns rendering off
	bne :+
		lda #%00000000
		sta $2001
		ldx #0
		stx nmi_ready
		jmp @ppu_update_end
	:
	; sprite OAM DMA
	ldx #0
	stx $2003
	lda #>oam
	sta $4014
	; palettes
	lda #%10001000
	sta $2000 ; set horizontal nametable increment
	lda $2002
	lda #$3F
	sta $2006
	stx $2006 ; set PPU address to $3F00
	ldx #0
	:
		lda palette, X
		sta $2007
		inx
		cpx #32
		bcc :-
	; nametable update
	ldx #0
	cpx nmt_update_len
	bcs @scroll
	@nmt_update_loop:
		lda nmt_update, X
		sta $2006
		inx
		lda nmt_update, X
		sta $2006
		inx
		lda nmt_update, X
		sta $2007
		inx
		cpx nmt_update_len
		bcc @nmt_update_loop
	lda #0
	sta nmt_update_len
@scroll:
	lda scroll_nmt
	and #%00000011 ; keep only lowest 2 bits to prevent error
	ora #%10001000
	sta $2000
	lda scroll_x
	sta $2005
	lda scroll_y
	sta $2005
	; enable rendering
	lda #%00011110
	sta $2001
	; flag PPU update complete
	ldx #0
	stx nmi_ready
@ppu_update_end:
	; if this engine had music/sound, this would be a good place to play it
	; unlock re-entry flag
	lda #0
	sta nmi_lock
@nmi_end:
	; restore registers and return
	pla
	tay
	pla
	tax
	pla
	rti

;
; irq
;

.segment "CODE"
irq:
	rti

;
; drawing utilities
;

.segment "CODE"

; ppu_update: waits until next NMI, turns rendering on (if not already), uploads OAM, palette, and nametable update to PPU
ppu_update:
	lda #1
	sta nmi_ready
	:
		lda nmi_ready
		bne :-
	rts

; ppu_skip: waits until next NMI, does not update PPU
ppu_skip:
	lda nmi_count
	:
		cmp nmi_count
		beq :-
	rts

; ppu_off: waits until next NMI, turns rendering off (now safe to write PPU directly via $2007)
ppu_off:
	lda #2
	sta nmi_ready
	:
		lda nmi_ready
		bne :-
	rts

; ppu_address_tile: use with rendering off, sets memory address to tile at X/Y, ready for a $2007 write
;   Y =  0- 31 nametable $2000
;   Y = 32- 63 nametable $2400
;   Y = 64- 95 nametable $2800
;   Y = 96-127 nametable $2C00
ppu_address_tile:
	lda $2002 ; reset latch
	tya
	lsr
	lsr
	lsr
	ora #$20 ; high bits of Y + $20
	sta $2006
	tya
	asl
	asl
	asl
	asl
	asl
	sta temp
	txa
	ora temp
	sta $2006 ; low bits of Y + X
	rts

; ppu_update_tile: can be used with rendering on, sets the tile at X/Y to tile A next time you call ppu_update
ppu_update_tile:
	pha ; temporarily store A on stack
	txa
	pha ; temporarily store X on stack
	ldx nmt_update_len
	tya
	lsr
	lsr
	lsr
	ora #$20 ; high bits of Y + $20
	sta nmt_update, X
	inx
	tya
	asl
	asl
	asl
	asl
	asl
	sta temp
	pla ; recover X value (but put in A)
	ora temp
	sta nmt_update, X
	inx
	pla ; recover A value (tile)
	sta nmt_update, X
	inx
	stx nmt_update_len
	rts

; ppu_update_byte: like ppu_update_tile, but X/Y makes the high/low bytes of the PPU address to write
;    this may be useful for updating attribute tiles
ppu_update_byte:
	pha ; temporarily store A on stack
	tya
	pha ; temporarily store Y on stack
	ldy nmt_update_len
	txa
	sta nmt_update, Y
	iny
	pla ; recover Y value (but put in Y)
	sta nmt_update, Y
	iny
	pla ; recover A value (byte)
	sta nmt_update, Y
	iny
	sty nmt_update_len
	rts

;
; gamepad
;

PAD_A      = $01
PAD_B      = $02
PAD_SELECT = $04
PAD_START  = $08
PAD_U      = $10
PAD_D      = $20
PAD_L      = $40
PAD_R      = $80

;; ork animation
ANIM_IDLE	=	$00
ANIM_WALK	=	$01
ANIM_CLIMB	=	$02
ANIM_BURP	=	$03
ANIM_SLURP	=	$04
ANIM_FALL	=	$05

;; 	music notes
A1  = 0
AS1 = 1
B1  = 2
C1  = 3
CS1 = 4
D1  = 5
DS1 = 6
E1  = 7
F1  = 8
FS1 = 9
G1  = 10
GS1 = 11

A2  = 12
AS2 = 13
B2  = 14
C2  = 15
CS2 = 16
D2  = 17
DS2 = 18
E2  = 19
F2  = 20
FS2 = 21
G2  = 22
GS2 = 23

A3  = 24
AS3 = 25
B3  = 26
C3  = 27
CS3 = 28
D3  = 29
DS3 = 30
E3  = 31
F3  = 32
FS3 = 33
G3  = 34
GS3 = 35

A4  = 36
AS4 = 37
B4  = 38
C4  = 39
CS4 = 40
D4  = 41
DS4 = 42
E4  = 43
F4  = 44
FS4 = 45
G4  = 46
GS4 = 47

A5  = 48
AS5 = 49
B5  = 50
C5  = 51
CS5 = 52
D5  = 53
DS5 = 54
E5  = 55
F5  = 56
FS5 = 57
G5  = 58
GS5 = 59
	
	;; todo finish


.segment "ZEROPAGE"
gamepad: .res 1

.segment "CODE"
; gamepad_poll: this reads the gamepad state into the variable labelled "gamepad"
;   This only reads the first gamepad, and also if DPCM samples are played they can
;   conflict with gamepad reading, which may give incorrect results.
gamepad_poll:
	; strobe the gamepad to latch current button state
	lda #1
	sta $4016
	lda #0
	sta $4016
	; read 8 bytes from the interface at $4016
	ldx #8
	:
		pha
		lda $4016
		; combine low two bits and store in carry bit
		and #%00000011
		cmp #%00000001
		pla
		; rotate carry into gamepad variable
		ror
		dex
		bne :-
	sta gamepad
	rts

;
; main
;

.segment "RODATA"
example_palette:
.byte $0F,$15,$26,$37 ; bg0 purple/pink
.byte $0F,$09,$19,$29 ; bg1 green
.byte $0F,$01,$11,$21 ; bg2 blue
.byte $0F,$00,$10,$30 ; bg3 greyscale
.byte $0F,$18,$28,$38 ; sp0 yellow
.byte $0F,$14,$24,$34 ; sp1 purple
.byte $0F,$1B,$2B,$3B ; sp2 teal
.byte $0F,$12,$22,$32 ; sp3 marine

bub_palette:
.byte $11,$30,$32,$0f ; bg0 text/bubbles/flagpole
.byte $11,$16,$05,$0f ; bg1 brick
.byte $11,$30,$28,$0f ; bg2 key/lock
.byte $11,$36,$18,$0f ; bg3 wood
.byte $11,$30,$10,$0f ; sp1 eyes/bubbles  10/32
.byte $11,$2a,$19,$0f ; sp0 bub
.byte $11,$18,$28,$0f ; sp2 key
.byte $11,$36,$18,$0f ; sp3 wood

bub_anim_idle_1:
	.byte $00,$00,$01,$00 ;y, tile, attr, x
	.byte $00,$01,$01,$08
	.byte $00,$02,$01,$10
	.byte $00,$03,$01,$18
	.byte $08,$10,$01,$00 ;row 2/4
	.byte $08,$11,$01,$08
	.byte $08,$12,$01,$10
	.byte $08,$13,$01,$18
	.byte $10,$20,$01,$00 ;row 3/4
	.byte $10,$21,$01,$08
	.byte $10,$22,$01,$10
	.byte $10,$23,$01,$18
	.byte $18,$30,$01,$00 ;row 4/4
	.byte $18,$31,$01,$08
	.byte $18,$32,$01,$10
	.byte $18,$33,$01,$18
	.byte $06,$0a,$00,$06 ;eyes
	.byte $06,$0b,$00,$0e
bub_anim_idle_2:
	.byte $00,$04,$01,$00 ;y, tile, attr, x
	.byte $00,$05,$01,$08
	.byte $00,$06,$01,$10
	.byte $00,$07,$01,$18
	.byte $08,$14,$01,$00 ;row 2/4
	.byte $08,$15,$01,$08
	.byte $08,$16,$01,$10
	.byte $08,$17,$01,$18
	.byte $10,$24,$01,$00 ;row 3/4
	.byte $10,$25,$01,$08
	.byte $10,$26,$01,$10
	.byte $10,$27,$01,$18
	.byte $18,$34,$01,$00 ;row 4/4
	.byte $18,$35,$01,$08
	.byte $18,$36,$01,$10
	.byte $18,$37,$01,$18
	.byte $07,$0a,$00,$06 ;eyes
	.byte $07,$0b,$00,$0e
bub_anim_idle_3:
	.byte $00,$08,$01,$00 ;y, tile, attr, x
	.byte $00,$09,$01,$08
	.byte $00,$80,$01,$10
	.byte $00,$80,$01,$18
	.byte $08,$18,$01,$00 ;row 2/4
	.byte $08,$19,$01,$08
	.byte $08,$1a,$01,$10
	.byte $08,$1b,$01,$18
	.byte $10,$28,$01,$00 ;row 3/4
	.byte $10,$29,$01,$08
	.byte $10,$2a,$01,$10
	.byte $10,$2b,$01,$18
	.byte $18,$38,$01,$00 ;row 4/4
	.byte $18,$39,$01,$08
	.byte $18,$3a,$01,$10
	.byte $18,$3b,$01,$18
	.byte $08,$0a,$00,$06 ;eyes
	.byte $08,$0b,$00,$0e

bub_anim_idle_times:
	.byte 20,4,20,4
	.byte 0
bub_anim_idle_LO:
	.byte <bub_anim_idle_1
	.byte <bub_anim_idle_2
	.byte <bub_anim_idle_3
	.byte <bub_anim_idle_2
bub_anim_idle_HI:
	.byte >bub_anim_idle_1
	.byte >bub_anim_idle_2
	.byte >bub_anim_idle_3
	.byte >bub_anim_idle_2

bub_anim_walk_1:
	.byte $00,$00,$01,$00 ;y, tile, attr, x
	.byte $00,$01,$01,$08
	.byte $00,$02,$01,$10
	.byte $00,$03,$01,$18
	.byte $08,$10,$01,$00 ;row 2/4
	.byte $08,$11,$01,$08
	.byte $08,$12,$01,$10
	.byte $08,$13,$01,$18
	.byte $10,$40,$01,$00 ;row 3/4
	.byte $10,$41,$01,$08
	.byte $10,$42,$01,$10
	.byte $10,$43,$01,$18
	.byte $18,$50,$01,$00 ;row 4/4
	.byte $18,$51,$01,$08
	.byte $18,$52,$01,$10
	.byte $18,$52,$01,$10
	.byte $06,$0a,$40,$07 ;eyes
	.byte $05,$0b,$80,$0e
bub_anim_walk_2:
	.byte $00,$04,$01,$00 ;y, tile, attr, x
	.byte $00,$05,$01,$08
	.byte $00,$06,$01,$10
	.byte $00,$07,$01,$18
	.byte $08,$14,$01,$00 ;row 2/4
	.byte $08,$15,$01,$08
	.byte $08,$16,$01,$10
	.byte $08,$17,$01,$18
	.byte $10,$44,$01,$00 ;row 3/4
	.byte $10,$45,$01,$08
	.byte $10,$46,$01,$10
	.byte $10,$47,$01,$18
	.byte $18,$54,$01,$00 ;row 4/4
	.byte $18,$55,$01,$08
	.byte $18,$56,$01,$10
	.byte $18,$56,$01,$10
	.byte $07,$0a,$40,$07 ;eyes
	.byte $06,$0b,$80,$0e
bub_anim_walk_3:
	.byte $00,$08,$01,$00 ;y, tile, attr, x
	.byte $00,$09,$01,$08
	.byte $00,$80,$01,$10
	.byte $00,$80,$01,$18
	.byte $08,$18,$01,$00 ;row 2/4
	.byte $08,$19,$01,$08
	.byte $08,$1a,$01,$10
	.byte $08,$1b,$01,$18
	.byte $10,$48,$01,$00 ;row 3/4
	.byte $10,$49,$01,$08
	.byte $10,$4a,$01,$10
	.byte $10,$4b,$01,$18
	.byte $18,$58,$01,$00 ;row 4/4
	.byte $18,$59,$01,$08
	.byte $18,$5a,$01,$10
	.byte $18,$5b,$01,$18
	.byte $08,$0a,$40,$07 ;eyes
	.byte $07,$0b,$80,$0e
bub_anim_walk_4:
	.byte $00,$04,$01,$00 ;y, tile, attr, x
	.byte $00,$05,$01,$08
	.byte $00,$06,$01,$10
	.byte $00,$07,$01,$18
	.byte $08,$14,$01,$00 ;row 2/4
	.byte $08,$15,$01,$08
	.byte $08,$16,$01,$10
	.byte $08,$17,$01,$18
	.byte $10,$4c,$01,$00 ;row 3/4
	.byte $10,$4d,$01,$08
	.byte $10,$4e,$01,$10
	.byte $10,$4f,$01,$18
	.byte $18,$5c,$01,$00 ;row 4/4
	.byte $18,$5d,$01,$08
	.byte $18,$5e,$01,$10
	.byte $18,$5f,$01,$18
	.byte $07,$0a,$40,$07 ;eyes
	.byte $06,$0b,$80,$0e
bub_anim_walk_5:
	.byte $00,$00,$01,$00 ;y, tile, attr, x
	.byte $00,$01,$01,$08
	.byte $00,$02,$01,$10
	.byte $00,$03,$01,$18
	.byte $08,$10,$01,$00 ;row 2/4
	.byte $08,$11,$01,$08
	.byte $08,$12,$01,$10
	.byte $08,$13,$01,$18
	.byte $10,$60,$01,$00 ;row 3/4
	.byte $10,$61,$01,$08
	.byte $10,$62,$01,$10
	.byte $10,$63,$01,$18
	.byte $18,$70,$01,$00 ;row 4/4
	.byte $18,$71,$01,$08
	.byte $18,$72,$01,$10
	.byte $18,$72,$01,$10
	.byte $06,$0a,$40,$07 ;eyes
	.byte $05,$0b,$80,$0e
bub_anim_walk_6:
	.byte $00,$04,$01,$00 ;y, tile, attr, x
	.byte $00,$05,$01,$08
	.byte $00,$06,$01,$10
	.byte $00,$07,$01,$18
	.byte $08,$14,$01,$00 ;row 2/4
	.byte $08,$15,$01,$08
	.byte $08,$16,$01,$10
	.byte $08,$17,$01,$18
	.byte $10,$64,$01,$00 ;row 3/4
	.byte $10,$65,$01,$08
	.byte $10,$66,$01,$10
	.byte $10,$67,$01,$18
	.byte $18,$74,$01,$00 ;row 4/4
	.byte $18,$75,$01,$08
	.byte $18,$76,$01,$10
	.byte $18,$76,$01,$10
	.byte $07,$0a,$40,$07 ;eyes
	.byte $06,$0b,$80,$0e
bub_anim_walk_7:
	.byte $00,$08,$01,$00 ;y, tile, attr, x
	.byte $00,$09,$01,$08
	.byte $00,$80,$01,$10
	.byte $00,$80,$01,$18
	.byte $08,$18,$01,$00 ;row 2/4
	.byte $08,$19,$01,$08
	.byte $08,$1a,$01,$10
	.byte $08,$1b,$01,$18
	.byte $10,$68,$01,$00 ;row 3/4
	.byte $10,$69,$01,$08
	.byte $10,$6a,$01,$10
	.byte $10,$6b,$01,$18
	.byte $18,$78,$01,$00 ;row 4/4
	.byte $18,$79,$01,$08
	.byte $18,$7a,$01,$10
	.byte $18,$7b,$01,$18
	.byte $08,$0a,$40,$07 ;eyes
	.byte $07,$0b,$80,$0e
bub_anim_walk_8:
	.byte $00,$04,$01,$00 ;y, tile, attr, x
	.byte $00,$05,$01,$08
	.byte $00,$06,$01,$10
	.byte $00,$07,$01,$18
	.byte $08,$14,$01,$00 ;row 2/4
	.byte $08,$15,$01,$08
	.byte $08,$16,$01,$10
	.byte $08,$17,$01,$18
	.byte $10,$6c,$01,$00 ;row 3/4
	.byte $10,$6d,$01,$08
	.byte $10,$6e,$01,$10
	.byte $10,$6f,$01,$18
	.byte $18,$7c,$01,$00 ;row 4/4
	.byte $18,$7d,$01,$08
	.byte $18,$7e,$01,$10
	.byte $18,$7f,$01,$18
	.byte $07,$0a,$40,$07 ;eyes
	.byte $06,$0b,$80,$0e

bub_anim_walk_times:
	.byte 4,4,6,4,4,4,6,4
	.byte 0
bub_anim_walk_LO:
	.byte <bub_anim_walk_1
	.byte <bub_anim_walk_2
	.byte <bub_anim_walk_3
	.byte <bub_anim_walk_4
	.byte <bub_anim_walk_5
	.byte <bub_anim_walk_6
	.byte <bub_anim_walk_7
	.byte <bub_anim_walk_8
bub_anim_walk_HI:
	.byte >bub_anim_walk_1
	.byte >bub_anim_walk_2
	.byte >bub_anim_walk_3
	.byte >bub_anim_walk_4
	.byte >bub_anim_walk_5
	.byte >bub_anim_walk_6
	.byte >bub_anim_walk_7
	.byte >bub_anim_walk_8

bub_anim_climb_1:
	.byte $00,$80,$01,$00 ;y, tile, attr, x
	.byte $00,$81,$01,$08
	.byte $00,$82,$01,$10
	.byte $00,$83,$01,$18
	.byte $08,$90,$01,$00 ;row 2/4
	.byte $08,$91,$01,$08
	.byte $08,$92,$01,$10
	.byte $08,$93,$01,$18
	.byte $10,$a0,$01,$00 ;row 3/4
	.byte $10,$a1,$01,$08
	.byte $10,$a2,$01,$10
	.byte $10,$a3,$01,$18
	.byte $18,$b0,$01,$00 ;row 4/4
	.byte $18,$b1,$01,$08
	.byte $18,$b2,$01,$10
	.byte $18,$b3,$01,$18
	.byte 011,$53,$80,004 ;eyes
	.byte 006,$77,$00,008
bub_anim_climb_2:
	.byte $00,$85,$01,$08 ;y, tile, attr, x
	.byte $00,$85,$01,$08
	.byte $00,$86,$01,$10
	.byte $00,$87,$01,$18
	.byte $08,$94,$01,$00 ;row 2/4
	.byte $08,$95,$01,$08
	.byte $08,$96,$01,$10
	.byte $08,$97,$01,$18
	.byte $10,$a4,$01,$00 ;row 3/4
	.byte $10,$a5,$01,$08
	.byte $10,$a6,$01,$10
	.byte $10,$a7,$01,$18
	.byte $18,$b4,$01,$00 ;row 4/4
	.byte $18,$b5,$01,$08
	.byte $18,$b6,$01,$10
	.byte $18,$b7,$01,$18
	.byte 011,$53,$80,005 ;eyes
	.byte 006,$77,$00,009
bub_anim_climb_3:
	.byte $00,$89,$01,$08 ;y, tile, attr, x
	.byte $00,$89,$01,$08
	.byte $00,$8a,$01,$10
	.byte $00,$8b,$01,$18
	.byte $08,$94,$01,$00 ;row 2/4
	.byte $08,$99,$01,$08
	.byte $08,$9a,$01,$10
	.byte $08,$9b,$01,$18
	.byte $10,$a4,$01,$00 ;row 3/4
	.byte $10,$a9,$01,$08
	.byte $10,$aa,$01,$10
	.byte $10,$ab,$01,$18
	.byte $18,$b4,$01,$00 ;row 4/4
	.byte $18,$b9,$01,$08
	.byte $18,$ba,$01,$10
	.byte $18,$ba,$01,$10
	.byte 011,$53,$80,005 ;eyes
	.byte 006,$77,$00,009
bub_anim_climb_4:
	.byte $00,$8c,$01,$00 ;y, tile, attr, x
	.byte $00,$8d,$01,$08
	.byte $00,$8e,$01,$10
	.byte $00,$8f,$01,$18
	.byte $08,$9c,$01,$00 ;row 2/4
	.byte $08,$9d,$01,$08
	.byte $08,$9e,$01,$10
	.byte $08,$9f,$01,$18
	.byte $10,$ac,$01,$00 ;row 3/4
	.byte $10,$ad,$01,$08
	.byte $10,$ae,$01,$10
	.byte $10,$af,$01,$18
	.byte $18,$bc,$01,$00 ;row 4/4
	.byte $18,$bd,$01,$08
	.byte $18,$be,$01,$10
	.byte $18,$bf,$01,$18
	.byte 011,$53,$80,006 ;eyes
	.byte 006,$77,$00,010

bub_anim_climb_times:
	.byte 6,3,3,6,3,3
	.byte 0
bub_anim_climb_LO:
	.byte <bub_anim_climb_1
	.byte <bub_anim_climb_2
	.byte <bub_anim_climb_3
	.byte <bub_anim_climb_4
	.byte <bub_anim_climb_3
	.byte <bub_anim_climb_2
bub_anim_climb_HI:
	.byte >bub_anim_climb_1
	.byte >bub_anim_climb_2
	.byte >bub_anim_climb_3
	.byte >bub_anim_climb_4
	.byte >bub_anim_climb_3
	.byte >bub_anim_climb_2

bub_anim_fall_1:
	.byte $00,$cc,$01,$00 ;y, tile, attr, x
	.byte $00,$cd,$01,$08
	.byte $00,$ce,$01,$10
	.byte $00,$cf,$01,$18
	.byte $08,$dc,$01,$00 ;row 2/4
	.byte $08,$dd,$01,$08
	.byte $08,$de,$01,$10
	.byte $08,$df,$01,$18
	.byte $10,$ec,$01,$00 ;row 3/4
	.byte $10,$ed,$01,$08
	.byte $10,$ee,$01,$10
	.byte $10,$ef,$01,$18
	.byte $18,$fc,$01,$00 ;row 4/4
	.byte $18,$fd,$01,$08
	.byte $18,$fe,$01,$10
	.byte $18,$ff,$01,$18
	.byte $06,$77,$00,$0c ;eyes
	.byte $0c,$57,$80,$05
bub_anim_fall_2:
	.byte $00,$cc,%01000000+1,$18 ;y, tile, attr, x
	.byte $00,$cd,%01000000+1,$10
	.byte $00,$ce,%01000000+1,$08
	.byte $00,$cf,%01000000+1,$00
	.byte $08,$dc,%01000000+1,$18 ;row 2/4
	.byte $08,$dd,%01000000+1,$10
	.byte $08,$de,%01000000+1,$08
	.byte $08,$df,%01000000+1,$00
	.byte $10,$ec,%01000000+1,$18 ;row 3/4
	.byte $10,$ed,%01000000+1,$10
	.byte $10,$ee,%01000000+1,$08
	.byte $10,$ef,%01000000+1,$00
	.byte $18,$fc,%01000000+1,$18 ;row 4/4
	.byte $18,$fd,%01000000+1,$10
	.byte $18,$fe,%01000000+1,$08
	.byte $18,$ff,%01000000+1,$00
	.byte $06,$77,%01000000+0,$0c;eyes
	.byte $0c,$57,%11000000+0,$12
bub_anim_fall_3:
	.byte $18,$cc,%11000000+1,$18 ;y, tile, attr, x
	.byte $18,$cd,%11000000+1,$10
	.byte $18,$ce,%11000000+1,$08
	.byte $18,$cf,%11000000+1,$00
	.byte $10,$dc,%11000000+1,$18 ;row 2/4
	.byte $10,$dd,%11000000+1,$10
	.byte $10,$de,%11000000+1,$08
	.byte $10,$df,%11000000+1,$00
	.byte $08,$ec,%11000000+1,$18 ;row 3/4
	.byte $08,$ed,%11000000+1,$10
	.byte $08,$ee,%11000000+1,$08
	.byte $08,$ef,%11000000+1,$00
	.byte $00,$fc,%11000000+1,$18 ;row 4/4
	.byte $00,$fd,%11000000+1,$10
	.byte $00,$fe,%11000000+1,$08
	.byte $00,$ff,%11000000+1,$00
	.byte $13,$77,%11000000+0,$0d;eyes
	.byte $0c,$57,%01000000+0,$12
bub_anim_fall_4:
	.byte $18,$cc,%10000000+1,$00 ;y, tile, attr, x
	.byte $18,$cd,%10000000+1,$08
	.byte $18,$ce,%10000000+1,$10
	.byte $18,$cf,%10000000+1,$18
	.byte $10,$dc,%10000000+1,$00 ;row 2/4
	.byte $10,$dd,%10000000+1,$08
	.byte $10,$de,%10000000+1,$10
	.byte $10,$df,%10000000+1,$18
	.byte $08,$ec,%10000000+1,$00 ;row 3/4
	.byte $08,$ed,%10000000+1,$08
	.byte $08,$ee,%10000000+1,$10
	.byte $08,$ef,%10000000+1,$18
	.byte $00,$fc,%10000000+1,$00 ;row 4/4
	.byte $00,$fd,%10000000+1,$08
	.byte $00,$fe,%10000000+1,$10
	.byte $00,$ff,%10000000+1,$18
	.byte $13,$77,%10000000+0,$0c ;eyes
	.byte $0c,$57,%00000000+0,$05

bub_anim_fall_times:
	.byte 10,10,10,10
	.byte 0
bub_anim_fall_LO:
	.byte <bub_anim_fall_2
	.byte <bub_anim_fall_3
	.byte <bub_anim_fall_4
	.byte <bub_anim_fall_1
bub_anim_fall_HI:
	.byte >bub_anim_fall_2
	.byte >bub_anim_fall_3
	.byte >bub_anim_fall_4
	.byte >bub_anim_fall_1

map_1_1:
	.byte ' ',' ',' ',' ',' ',' ',' '
	.byte ' ','>',' ',' ',' ',' ',' '
	.byte '#','#',' ',' ',' ',' ','4'
	.byte ' ',' ',' ',' ',' ',' ','#'
	.byte 'o','o',' ','0',' ','#','#'
	.byte '#','#','#','#','#','#','#'
map_1_2:
	.byte ' ',' ','#',' ',' ',' ',' '
	.byte ' ',' ',' ',' ',' ','4',' '
	.byte ' ','H','#',' ','H','#',' '
	.byte ' ','H','#',' ','H',' ',' '
	.byte ' ','H','#',' ',' ',' ',' '
	.byte '0','H','#',' ',' ','o','o'
map_1_3:
	.byte ' ',' ',' ',' ',' ',' ',' '
	.byte ' ',' ',' ',' ',' ',' ',' '
	.byte ' ',' ','0',' ',' ',' ',' '
	.byte ' ',' ','o',' ',' ',' ','4'
	.byte ' ','o','o','o',' ',' ','#'
	.byte 'o','o','o','o','o',' ',' '
map_1_4:
	.byte '4',' ',' ',' ',' ',' ',' '
	.byte '#','H',' ','#',' ','o',' '
	.byte ' ','H',' ','#',' ','o',' '
	.byte '#','#','#','#','#','#','H'
	.byte ' ',' ',' ',' ',' ',' ','H'
	.byte 'o',' ','0','#',' ',' ','H'

map_0_0:
	.byte ' ',' ',' ',' ',' ',' ',' '
	.byte ' ',' ',' ',' ',' ',' ',' '
	.byte ' ',' ',' ',' ',' ',' ',' '
	.byte ' ',' ',' ',' ',' ',' ',' '
	.byte ' ',' ',' ',' ',' ',' ',' '
	.byte ' ',' ',' ',' ',' ',' ',' '

tune:
	.byte C4,E4,G4,C5


.segment "ZEROPAGE"
cursor_x: .res 1
cursor_y: .res 1
temp_x:   .res 1
temp_y:   .res 1
anim:		.res 1				;index for set_animation
anim_curr:	.word 0				;current animation (timings)
fr_curr_LO:	.word 0				;current frameset LO
fr_curr_HI:	.word 0				;current frameset HI
anim_fr:	.res 1				;current frame
anim_cd:	.res 1 				;countdown to next anim frame
ptr:		.word 0
faceleft:	.res 1				;facing left
tuneidx:	.res 1
tunenote:	.res 1

.segment "CODE"
main:
	; setup 
	ldx #0
	:
		lda bub_palette, X
		sta palette, X
		inx
		cpx #32
		bcc :-
	jsr setup_background
	; center the cursor
	lda #128
	sta cursor_x
	lda #120
	sta cursor_y

	jsr init_apu
	lda #0
	sta tuneidx

	;;  initialize to idle animation
	lda #0
	sta anim
	jsr set_animation

	; show the screen
;;	jsr draw_ork				
	jsr ppu_update
	; main loop
@loop:
	; read gamepad
	jsr gamepad_poll
	; respond to gamepad state
	lda gamepad
	and #PAD_START
	beq :+
		jsr push_start
		jmp @draw ; start clobbers everything, don't check other buttons
	:
	jsr release_start ; releasing start restores scroll
	lda gamepad
	and #PAD_U
	beq :+
		jsr push_u
	:
	lda gamepad
	and #PAD_D
	beq :+
		jsr push_d
	:
	lda gamepad
	and #PAD_L
	beq :+
		jsr push_l
	:
	lda gamepad
	and #PAD_R
	beq :+
		jsr push_r
	:
	lda gamepad
	and #PAD_SELECT
	beq :+
		jsr push_select
	:
	lda gamepad
	and #PAD_B
	beq :+
		jsr push_b
	:
	lda gamepad
	and #PAD_A
	beq :+
		jsr push_a
	:

	;; set animation to idle if not l/r/u/d
	lda #PAD_L
	ora #PAD_R
	ora #PAD_U
	ora #PAD_D
	and gamepad
	bne :+
		lda #ANIM_IDLE
		jsr set_anim_a
	:
@draw:
	; draw everything and finish the frame

	jsr draw_ork
	jsr ppu_update
	; keep doing this forever!
	jmp @loop

push_u:
	dec cursor_y

	lda #nmi_count
	and $01
	bne :+
		dec cursor_y
	:

	; Y wraps at 240
	lda cursor_y
	cmp #240
	bcc :+
		lda #239
		sta cursor_y
	:
	;; set animation to climb
	lda #ANIM_CLIMB
	jsr set_anim_a
	rts

push_d:
	inc cursor_y
	inc cursor_y

	; fall noise
	lda cursor_y
	sta $4002
	

	; Y wraps at 240
	lda cursor_y
	cmp #240
	bcc :+
		lda #0
		sta cursor_y
	:
	;; set animation to climb
	lda #ANIM_FALL
	jsr set_anim_a
	rts

push_l:
	lda #%01000000
	sta faceleft
	dec cursor_x

	lda #nmi_count
	and $01
	bne :+
		dec cursor_x
	:

	;; set animation to walk
	lda #ANIM_WALK
	jsr set_anim_a
	rts

push_r:
	lda #0
	sta faceleft
	inc cursor_x

	lda #nmi_count
	and $01
	bne :+
		inc cursor_x
	:

	;; set animation to walk
	lda #ANIM_WALK
	jsr set_anim_a
	rts

push_select:
	; turn off rendering so we can manually update entire nametable
	jsr ppu_off
	jsr setup_background
	; wait for user to release select before continuing
	:
		jsr gamepad_poll
		lda gamepad
		and #PAD_SELECT
		bne :-
	rts

push_start:
	inc scroll_x
	inc scroll_y
	; Y wraps at 240
	lda scroll_y
	cmp #240
	bcc :+
		lda #0
		sta scroll_y
	:
	; when X rolls over, toggle the high bit of nametable select
	lda scroll_x
	bne :+
		lda scroll_nmt
		eor #$01
		sta scroll_nmt
	:
	rts

release_start:
	lda #0
	sta scroll_x
	sta scroll_y
	sta scroll_nmt
	rts

push_b:
	;; shut up
	lda #%10110000
	sta $4000
	
	jsr snap_cursor
	lda cursor_x
	lsr
	lsr
	lsr
	tax ; X = cursor_x / 8
	lda cursor_y
	lsr
	lsr
	lsr
	tay ; Y = cursor_y / 8
	lda #4
	jsr ppu_update_tile ; puts tile 4 at X/Y
	rts

push_a:
	;;  make noise
	;; asdf
	ldx tuneidx				; "tune" index
	lda tune, X
	sta tunenote
	ldy tunenote
	lda periodTableLO, Y
	sta $4002
	lda periodTableHI, Y
	sta $4003
	inc tuneidx
	lda #3
	cmp tuneidx
	bne :+
		lda #0
		sta tuneidx
	:
	lda #%10111111
	sta $4000
	
	;lda nmi_count
	;sta $4003
	
	;lda #%10111111
	;sta $4000
	
	;; example started here
	jsr snap_cursor
	lda cursor_x
	lsr
	lsr
	lsr
	sta temp_x ; cursor_x / 8
	lda cursor_y
	lsr
	lsr
	lsr
	sta temp_y ; cursor_y / 8
	; draw a ring of 8 tiles around the cursor
	dec temp_x ; x-1
	dec temp_y ; y-1
	ldx temp_x
	ldy temp_y
	lda #5
	jsr ppu_update_tile
	inc temp_x ; x
	ldx temp_x
	ldy temp_y
	lda #6
	jsr ppu_update_tile
	inc temp_x ; x+1
	ldx temp_x
	ldy temp_y
	lda #5
	jsr ppu_update_tile
	dec temp_x
	dec temp_x ; x-1
	inc temp_y ; y
	ldx temp_x
	ldy temp_y
	lda #6
	jsr ppu_update_tile
	inc temp_x
	inc temp_x ; x+1
	ldx temp_x
	ldy temp_y
	lda #6
	jsr ppu_update_tile
	dec temp_x
	dec temp_x ; x-1
	inc temp_y ; y+1
	ldx temp_x
	ldy temp_y
	lda #5
	jsr ppu_update_tile
	inc temp_x ; x
	ldx temp_x
	ldy temp_y
	lda #6
	jsr ppu_update_tile
	inc temp_x ; x+1
	ldx temp_x
	ldy temp_y
	lda #5
	jsr ppu_update_tile
	rts

; snap_cursor: snap cursor to nearest tile
snap_cursor:
	lda cursor_x
	clc
	adc #4
	and #$F8
	sta cursor_x
	lda cursor_y
	clc
	adc #4
	and #$F8
	sta cursor_y
	; Y wraps at 240
	cmp #240
	bcc :+
		lda #0
		sta cursor_y
	:
	rts

draw_cursor:
	; four sprites centred around the currently selected tile
	; y position (note, needs to be one line higher than sprite's appearance)
	lda cursor_y
	sec
	sbc #5 ; Y-5
	sta oam+(0*4)+0
	sta oam+(1*4)+0
	lda cursor_y
	clc
	adc #3 ; Y+3
	sta oam+(2*4)+0
	sta oam+(3*4)+0
	; tile
	lda #1
	sta oam+(0*4)+1
	sta oam+(1*4)+1
	sta oam+(2*4)+1
	sta oam+(3*4)+1
	; attributes
	lda #%00000000 ; no flip
	sta oam+(0*4)+2
	lda #%01000000 ; horizontal flip
	sta oam+(1*4)+2
	lda #%10000000 ; vertical flip
	sta oam+(2*4)+2
	lda #%11000000 ; both flip
	sta oam+(3*4)+2
	; x position
	lda cursor_x
	sec
	sbc #4 ; X-4
	sta oam+(0*4)+3
	sta oam+(2*4)+3
	lda cursor_x
	clc
	adc #4 ; X+4
	sta oam+(1*4)+3
	sta oam+(3*4)+3
	rts

	;; set up ork animation pointers/vars based on 'anim'
set_animation:
	lda anim
	cmp #ANIM_IDLE
	bne :+
		lda #<bub_anim_idle_times
		sta anim_curr
		lda #>bub_anim_idle_times
		sta anim_curr+1
		lda #<bub_anim_idle_LO
		sta fr_curr_LO
		lda #>bub_anim_idle_LO
		sta fr_curr_LO+1
		lda #<bub_anim_idle_HI
		sta fr_curr_HI
		lda #>bub_anim_idle_HI
		sta fr_curr_HI+1
		jmp animdone
	:
	cmp #ANIM_WALK
	bne :+
		lda #<bub_anim_walk_times
		sta anim_curr
		lda #>bub_anim_walk_times
		sta anim_curr+1
		lda #<bub_anim_walk_LO
		sta fr_curr_LO
		lda #>bub_anim_walk_LO
		sta fr_curr_LO+1
		lda #<bub_anim_walk_HI
		sta fr_curr_HI
		lda #>bub_anim_walk_HI
		sta fr_curr_HI+1
		jmp animdone
	:
	cmp #ANIM_CLIMB
	bne :+
		lda #<bub_anim_climb_times
		sta anim_curr
		lda #>bub_anim_climb_times
		sta anim_curr+1
		lda #<bub_anim_climb_LO
		sta fr_curr_LO
		lda #>bub_anim_climb_LO
		sta fr_curr_LO+1
		lda #<bub_anim_climb_HI
		sta fr_curr_HI
		lda #>bub_anim_climb_HI
		sta fr_curr_HI+1
		jmp animdone
	:
	cmp #ANIM_BURP
	bne :+
	:
	cmp #ANIM_SLURP
	bne :+
	:
	cmp #ANIM_FALL
	bne :+
		lda #<bub_anim_fall_times
		sta anim_curr
		lda #>bub_anim_fall_times
		sta anim_curr+1
		lda #<bub_anim_fall_LO
		sta fr_curr_LO
		lda #>bub_anim_fall_LO
		sta fr_curr_LO+1
		lda #<bub_anim_fall_HI
		sta fr_curr_HI
		lda #>bub_anim_fall_HI
		sta fr_curr_HI+1
		;jmp animdone
animdone:
	ldx #0
	stx anim_fr					;start on frame 0
	lda (anim_curr, X)			;initialize timing
	sta anim_cd
	rts

;; set anim based on accumulator, only if new
set_anim_a:
	cmp anim
	beq :+
		sta anim
		jsr set_animation

		lda anim
		cmp #ANIM_FALL			;make noise if falling
		bne :+
			lda #<250			;279
			sta $4002
			lda #>250
			sta $4003
			;lda nmi_count
			;sta $4002
			lda $00
			sta $4002
	;; $4000 = first pulse channel
	;; %DD11VVVV - Duty cycle, Volume
			lda #%10111111
			sta $4000
			rts
		:
		cmp #ANIM_IDLE
		bne :+
			;lda #%10110000				;stop being noisy  FIXME uncomment probably
			;sta $4000
	:
	rts

draw_ork:
	;; determine which frame to load
	;; first: is time for next frame?
	dec anim_cd
	lda #0
	cmp anim_cd
	beq @advframe
	jmp @loadframe
@advframe:
	inc anim_fr
	ldx anim_fr
	ldy anim_fr
	lda (anim_curr), Y
	cmp #0
	beq @resetframe
	jmp @setcountdown
@resetframe:
	ldx #0
	stx anim_fr
@setcountdown:
	ldy anim_fr
	lda (anim_curr), Y
	sta anim_cd

@loadframe:
	ldy anim_fr				; frame index
	lda (fr_curr_LO), Y
	sta ptr
	lda (fr_curr_HI), Y
	sta ptr+1

	;; draw all ork's tiles
	ldy #0				; for Y = 0 to 18*4
@draworktile:
		lda (ptr), Y		; y position (needs -1?)
		adc cursor_y
		sta oam, Y
		iny
		lda (ptr), Y		; tile
		sta oam, Y
		iny
		lda (ptr), Y		; attribute
		;;lda #%01000000 ; horizontal flip
		;;lda #%10000000 ; vertical flip
		;;lda #%11000000 ; both flip
		;;lda #%00000011 ; palette bits
		eor faceleft			;ora
		sta oam, Y
		iny
	;; subtract offsets if facing left
	    lda faceleft
		and #%01000000
		bne :+
			lda cursor_x	; x position (normal)
			adc (ptr), Y
			jmp @putxpos
		:
		lda cursor_x		; x position (flipped)
		adc #24
		sbc (ptr), Y
@putxpos:
		sta oam, Y
		iny
		cpy #(18*4) 	;18 tiles of 4 vals each
		bcc @draworktile
	rts

draw_bub:
	; 16 sprites
	; y position (needs -1, looks like)
	lda cursor_y
	;;sec
	;;sbc #1 ; Y-1
	;;clc
	sta oam+(0*4)+0
	sta oam+(1*4)+0
	sta oam+(2*4)+0
	sta oam+(3*4)+0

	lda cursor_y
	;;clc
	adc #8 ; Y+8
	sta oam+(4*4)+0
	sta oam+(5*4)+0
	sta oam+(6*4)+0
	sta oam+(7*4)+0

	lda cursor_y
	;;sec
	adc #16 ; Y+16
	sta oam+(8*4)+0
	sta oam+(9*4)+0
	sta oam+(10*4)+0
	sta oam+(11*4)+0

	lda cursor_y
	;;clc
	adc #24 ; Y+24
	sta oam+(12*4)+0
	sta oam+(13*4)+0
	sta oam+(14*4)+0
	sta oam+(15*4)+0

	lda cursor_y
	adc #6 						;eye1&2
	sta oam+(16*4)+0
	sta oam+(17*4)+0

	; tile
	;;lda #11						; #2  this changes which tile we use
	lda #0+(16*0)
	sta oam+(0*4)+1
	lda #1+(16*0)
	sta oam+(1*4)+1
	lda #2+(16*0)
	sta oam+(2*4)+1
	lda #3+(16*0)
	sta oam+(3*4)+1

	lda #0+(16*1)
	sta oam+(4*4)+1
	lda #1+(16*1)
	sta oam+(5*4)+1
	lda #2+(16*1)
	sta oam+(6*4)+1
	lda #3+(16*1)
	sta oam+(7*4)+1

	lda #0+(16*2)
	sta oam+(8*4)+1
	lda #1+(16*2)
	sta oam+(9*4)+1
	lda #2+(16*2)
	sta oam+(10*4)+1
	lda #3+(16*2)
	sta oam+(11*4)+1

	lda #0+(16*3)
	sta oam+(12*4)+1
	lda #1+(16*3)
	sta oam+(13*4)+1
	lda #2+(16*3)
	sta oam+(14*4)+1
	lda #3+(16*3)
	sta oam+(15*4)+1

	lda #10+(16*0)				;eye1
	sta oam+(16*4)+1
	lda #11+(16*0)				;eye2
	sta oam+(17*4)+1
	
	; attributes
	lda #%00000001 ; no flip
	;;lda #%01000000 ; horizontal flip
	;;lda #%10000000 ; vertical flip
	;;lda #%11000000 ; both flip
	;;lda #%00000011 ; palette bits
	sta oam+(0*4)+2
	sta oam+(1*4)+2
	sta oam+(2*4)+2
	sta oam+(3*4)+2
	sta oam+(4*4)+2
	sta oam+(5*4)+2
	sta oam+(6*4)+2
	sta oam+(7*4)+2
	sta oam+(8*4)+2
	sta oam+(9*4)+2
	sta oam+(10*4)+2
	sta oam+(11*4)+2
	sta oam+(12*4)+2
	sta oam+(13*4)+2
	sta oam+(14*4)+2
	sta oam+(15*4)+2
	lda #%00000000 				; pal 0
	sta oam+(16*4)+2
	sta oam+(17*4)+2

	; x position
	lda cursor_x
	sec
	sta oam+(0*4)+3
	sta oam+(4*4)+3
	sta oam+(8*4)+3
	sta oam+(12*4)+3

	lda cursor_x
	clc
	adc #8 ; X+8
	sta oam+(1*4)+3
	sta oam+(5*4)+3
	sta oam+(9*4)+3
	sta oam+(13*4)+3

	lda cursor_x
	clc
	adc #16 ; X+16
	sta oam+(2*4)+3
	sta oam+(6*4)+3
	sta oam+(10*4)+3
	sta oam+(14*4)+3

	lda cursor_x
	clc
	adc #24 ; X+24
	sta oam+(3*4)+3
	sta oam+(7*4)+3
	sta oam+(11*4)+3
	sta oam+(15*4)+3

	lda cursor_x
	clc
	adc #6
	sta oam+(16*4)+3			;eye1
	adc #8
	sta oam+(17*4)+3			;eye2

	rts

setup_background:
	; first nametable, start by clearing to empty
	lda $2002 ; reset latch
	lda #$20
	sta $2006
	lda #$00
	sta $2006
	; empty nametable
	lda #0
	ldy #30 ; 30 rows
	:
		ldx #32 ; 32 columns
		:
			sta $2007
			dex
			bne :-
		dey
		bne :--
	; set all attributes to 0
	ldx #64 ; 64 bytes
	:
		sta $2007
		dex
		bne :-
	; fill in an area in the middle with 1/2 checkerboard
	lda #1
	ldy #8 ; start at row 8
	:
		pha ; temporarily store A, it will be clobbered by ppu_address_tile routine
		ldx #8 ; start at column 8
		jsr ppu_address_tile
		pla ; recover A
		; write a line of checkerboard
		ldx #8
		:
			sta $2007
			eor #$3
			inx
			cpx #(32-8)
			bcc :-
		eor #$3
		iny
		cpy #(30-8)
		bcc :--
	
	;; background messing around
	ldy #8
	ldx #8
	jsr ppu_address_tile
	lda #'a'
	sta $2007
	
	; second nametable, fill with simple pattern
	lda #$24
	sta $2006
	lda #$00
	sta $2006
	lda #$00
	ldy #30
	:
		ldx #32
		:
			sta $2007
			clc
			adc #1
			and #3
			dex
			bne :-
		clc
		adc #1
		and #3
		dey
		bne :--
	; 4 stripes of attribute
	lda #0
	ldy #4
	:
		ldx #16
		:
			sta $2007
			dex
			bne :-
		clc
		adc #%01010101
		dey
		bne :--
	rts



play_pulse_scale:
	ldx #20
:       jsr play_pulse_note
	inx
	cpx #32
	bne :-
	rts

play_pulse_note:
	lda periodTableHI,x
	sta $4003
	
	lda periodTableLO,x
	sta $4002
	
	; Fade volume from 15 to 0
	ldy #15
:       tya
	ora #%10110000
	sta $4000
;	jsr delay_frame
	dey
	bpl :-
	
	rts

play_tri_scale:
	ldx #20
:       jsr play_tri_note
	inx
	cpx #32
	bne :-
	rts

play_tri_note:
	; Halve period, since triangle is octave lower
	lda periodTableHI,x
	lsr a
	sta $400B
	
	lda periodTableLO,x
	ror a
	sta $400A
	
	; Play for 8 frames, then silence for 8 frames
	lda #%11000000
	sta $4008
	sta $4017
	
;	ldy #8
;	jsr delay_y_frames
	
	lda #%10000000
	sta $4008
	sta $4017
	
;	ldy #8
;	jsr delay_y_frames			;
	
	rts

; NTSC period table generated by mktables.py. See
; http://wiki.nesdev.com/w/index.php/APU_period_table
periodTableLO:
  .byt $f1,$7f,$13,$ad,$4d,$f3,$9d,$4c,$00,$b8,$74,$34
  .byt $f8,$bf,$89,$56,$26,$f9,$ce,$a6,$80,$5c,$3a,$1a
  .byt $fb,$df,$c4,$ab,$93,$7c,$67,$52,$3f,$2d,$1c,$0c
  .byt $fd,$ef,$e1,$d5,$c9,$bd,$b3,$a9,$9f,$96,$8e,$86
  .byt $7e,$77,$70,$6a,$64,$5e,$59,$54,$4f,$4b,$46,$42
  .byt $3f,$3b,$38,$34,$31,$2f,$2c,$29,$27,$25,$23,$21
  .byt $1f,$1d,$1b,$1a,$18,$17,$15,$14

periodTableHI:
  .byt $07,$07,$07,$06,$06,$05,$05,$05,$05,$04,$04,$04
  .byt $03,$03,$03,$03,$03,$02,$02,$02,$02,$02,$02,$02
  .byt $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01
  .byt $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  .byt $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  .byt $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
  .byt $00,$00,$00,$00,$00,$00,$00,$00

	
;
; end of file
;
