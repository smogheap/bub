#!/bin/sh
mkdir bin
cd bin
rm bub.nes bub.o bub.map.txt bub.labels.txt bub.nes.dbg
img2chr ../work/chr-bg.png background.chr
img2chr ../work/chr-sp.png sprite.chr
ca65 ../src/bub.s -g -o bub.o
ld65 -o bub.nes -C ../src/bub.cfg bub.o -m bub.map.txt -Ln bub.labels.txt --dbgfile bub.nes.dbg
cd ..
